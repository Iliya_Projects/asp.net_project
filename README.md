# The OBS Remake. 
## This is a Manual/User Guide to how the system works and how it should be used.
## How to use the web page: 
1. Start the project with `LoginPage.aspx`.
2. Click on **Register** and create an account.
3. Login with your **Student ID** (you will be given an ID after you register) and your password. 
4. In the upper menu select **Sign up for lectures** to choose from a list of lectures for your faculty. 
5. Choose a lecture and click **Save**.

## What Pages are there: 

1. Pages: 
* `LoginPage.aspx` is the main page with which the user should start. From here on he/she will have the possibility to either **login**,**register** or see the **list of all the lectures**. 
* `Register.aspx` is the page designated to the registration process. The page is filled with **Text Input Boxes** for the different information and the **validations** that are needed to create a user.
* `StudentPage.aspx` is the page the user sees after **login** .
If the user was signed for some **lectures** a table of them will be shown. From this page the user will be able to do things like:  **sign up for a lecture**, **log out** or see the **list of all lectures** 
* `LectureList.aspx` shows the list of all lectures that are currently held in all of the faculties. 
* `AddLectures.aspx` shows a list of **lectures** that the user can sign up for. The lectures that are shown here are only lectures from the **faculty** on which the user is. 
## Functionality: 

1. `LoginPage.aspx` 
    * You should always load the site with the `LoginPage.aspx` and move from there on. If you try to load a different page an error will occur or the page won't be loaded. Pages like `Register.aspx`, `StudentPage.aspx` and `AddLecture.aspx` use sessions. 
    * The fields for ID and Password have both validators. The password doesn't need to be in any particular style but the ID can only be a 3 digit number. 
    * After log in the credentials will be checked and on success the user will be moved to the `StudentPage.aspx`. 
    * `Home` in the top menu will redirect the user the `LoginPage.aspx.`. On pages to which the user came after being loged in, `Home` will redirect to the `StudentPage.aspx` with the correct **student id**. 
    * `Lectures` in the menu will show a table with all of the lectures. 

2. `Register.aspx` 
    * The user has to fill all the fields and click on **Register** to create an account, after that a **student id** will be created by the system and shown. The user has to remember the **ID** in order to be able to **login**. **Cancel** clears all the fields.
3. `StudentPage.aspx` 
    * From here the user can choose to which lecture he/she wants to sign up for. The lectures that are shown are only form his/hers faculty only.
    * From here on `Home` in the top menu will redirect the user to `StudentPage.aspx` until the user has **loged out**.

4. `AddLectures.aspx` 
    * The user can choose from a list of lectures and after pressing **Save** ,the choice will be evaluated and if the user was already signed for one of the chosen lectures and error will be shown. 

5. `Home` 
    * If the user calls `Home` from `StudentPage.aspx` (after login) it will have a different functionality. This applies even if the user chooses `Lectures` and after that chooses `Home`. 
    * It will take the user back to his/hers `StudentPage.aspx`.

# Students who took part in this project - Iliya Bahchevanski Student Id:734126 E-Mail:Iliya.Bahchevanski@stud.h-da.de

**Created by Iliya Bahchevanski for the Course on Asp.Net by Mr. Pekka Alaluukas at University of Applied Science Darmstadt 2016**