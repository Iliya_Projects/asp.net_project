﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OBSProject
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private int studetnId;
        private string password;
        private string name;
        protected void Page_Load(object sender, EventArgs e)
        {

            HttpResponse.RemoveOutputCacheItem("/LoginPage.aspx");

        }
        protected void Login_Event(object sender, EventArgs e)
        {
            try
            {
                this.studetnId = int.Parse(this.TextBoxStudentID.Text);
                this.password = this.TextBoxPassword.Text;
                int studentId = 0;
                this.checkCredentials(ref studentId);
                Session["studentId"] = studentId.ToString();
                Session["name"] = this.name;
                Response.Redirect("StudentPage.aspx");

            }
            catch (Exception ecx)
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "error",
                    "<script language=javascript>alert('Unable to log in. Wrong credentials.');</script>");
                HttpResponse.RemoveOutputCacheItem("/LoginPage.aspx");
                this.clearTextFields();
            }

        }

        protected void checkCredentials(ref int studentId)
        {
            OBS_DatabaseEntities studentEntity = new OBS_DatabaseEntities();

            var getIDAndPwd = from student in studentEntity.Students
                              where student.pwd.Equals(this.password) &&
                              student.id_Student.Equals(this.studetnId)
                              select new { Password = student.pwd, Id = student.id_Student, Name = student.first_name };
            getIDAndPwd.ToList();

            studentId = getIDAndPwd.ToList().Last().Id;
            this.name = getIDAndPwd.ToList().Last().Name;
           
        }

        protected void clearTextFields()
        {
            this.TextBoxStudentID.Text = "";
            this.TextBoxPassword.Text = "";
        }


        protected void Register_Event(object sender, EventArgs e)
        {

            Session["id"] = "2";
            Response.Redirect("Register.aspx");
        }
    }
}