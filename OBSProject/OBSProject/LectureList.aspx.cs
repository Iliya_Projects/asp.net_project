﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace OBSProject
{
    public partial class LectureList : System.Web.UI.Page
    {
        private string studentId;
        private string studentName;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.checkSession();
            this.fillView();

        }
        protected void checkSession()
        {
            try
            {
                this.studentId = Session["studentId"].ToString();
                this.studentName = Session["name"].ToString();
                string sendetFrom = Session["sender"].ToString();

                switch (sendetFrom)
                {
                    //this case will be true even if the sender is the add lectures page
                    case "studentPage":
                        this.changeHomeURL();
                        break;
                }

            }
            catch (Exception)
            {
                //if we are here it means the sender was not a page after login
                //and we can continue as usual and go back to the login page
                
            }
        }

        protected void changeHomeURL()
        {
            MasterPage myMasterPage = (MasterPage)Page.Master;
            Menu masterMenu = (Menu)myMasterPage.FindControl("Menu1");
            MenuItem homeMenuItem = masterMenu.FindItem("HomeItem");
            homeMenuItem.NavigateUrl = "";
            masterMenu.MenuItemClick += MasterMenu_MenuItemClick;
        }

        private void MasterMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            string command = e.Item.Text;
            switch (command)
            {
                case "Home":
                    Session["studentId"] = this.studentId;
                    Session["name"] = this.studentName;
                    Response.Redirect("StudentPage.aspx");
                    break;
                default:
                    this.clearSessionAndCache();
                    Response.Redirect("LoginPage.aspx");
                    break;
            }
        }

        protected void fillView()
        {
            OBS_DatabaseEntities entity = new OBS_DatabaseEntities();

            var getListOfLectures = (from listAll in entity.ProfesorAndLectures
                                     join profs in entity.Profesors on listAll.id_Profesor equals profs.id_Profesor
                                     join lectures in entity.Lectures on listAll.id_lecture equals lectures.id_lecture
                                     select new
                                     {
                                         Lecture = lectures.name_lecture,
                                         Professor = profs.last_name,
                                         Faculty = lectures.faculty
                                     }).Distinct();

            GridLecturesAndProfs.DataSource = getListOfLectures.ToList();
            GridLecturesAndProfs.DataBind();
        }

        protected void GridLecturesAndProfs_PageIndexChanged(object sender, GridViewPageEventArgs e)
        {
            GridLecturesAndProfs.PageIndex = e.NewPageIndex;
            GridLecturesAndProfs.DataBind();
        }

        protected void clearSessionAndCache()
        {

            Session.Clear();
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            Response.AddHeader("Pragma", "no-cache");
            Response.AddHeader("Expires", "0");
            Response.Cache.SetValidUntilExpires(false);

        }

    }
}