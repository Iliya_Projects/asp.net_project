﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AddLectures.aspx.cs" Inherits="OBSProject.MyLectures" %>
<%@ Reference VirtualPath="/MasterPage.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <div class="container-fluid pageContainterMargin">
        <asp:GridView ID="GridView1" AutoGenerateColumns="false" CssClass="table"  DataKeyNames="ProfessorsId, LectureId" runat="server">
            <Columns>
                <asp:TemplateField  HeaderText="Selected ">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="LectureList" HeaderText="Lectures" SortExpression="LectureList" />            
                <asp:BoundField DataField="ProfessorList" HeaderText="Professors" SortExpression="ProfesorListSor" />
                <asp:BoundField DataField="ProfessorsId"  Visible="false"/>
                <asp:BoundField DataField="LectureId" Visible="false"/>
            </Columns>
        </asp:GridView>
        <br />
        <div class="container-fluid">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" />
        </div>
    </div>
</asp:Content>
