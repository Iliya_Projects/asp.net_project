﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OBSProject
{
    public partial class StudentPage : System.Web.UI.Page
    {
        private int studentID;
        private string name;
        private OBS_DatabaseEntities databaseEntity;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                this.databaseEntity = new OBS_DatabaseEntities();
                this.getStudentIDAndName();
                this.edditMasterPageMenu();
                this.populateLecturesTable();

            }
            catch (Exception)
            {
                this.clearSessionAndCache();
                Response.Redirect("LoginPage.aspx");
            }

        }

        protected void getStudentIDAndName()
        {
            try
            {
                this.studentID = int.Parse(Session["studentId"].ToString());
                this.name = Session["name"].ToString();
                
            }
            catch (Exception)
            {

                throw;
            }
        }
        protected void edditMasterPageMenu()
        {
            MasterPage myMasterPage = (MasterPage)Page.Master;
            Menu masterMenu = (Menu)myMasterPage.FindControl("Menu1");
            MenuItem homeMenuItem = masterMenu.FindItem("HomeItem");
            MenuItem lecturesMenuItem = masterMenu.FindItem("LecturesItem");
            masterMenu.Items.Add(new MenuItem { Text = "Sign up for Lectures" });
            masterMenu.Items.Add(new MenuItem { Text = "Logout" });
            homeMenuItem.NavigateUrl = "";
            lecturesMenuItem.NavigateUrl = "";
            masterMenu.MenuItemClick += MasterMenu_MenuItemClick;
        }

        private void MasterMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            string command = e.Item.Text;
            switch (command)
            {
                case "Sign up for Lectures":
                    Session["studentId"] = this.studentID.ToString();
                    Session["name"] = this.name;
                    Response.Redirect("AddLectures.aspx");
                    break;
                case "Logout":
                    this.clearSessionAndCache();
                    Response.Redirect("LoginPage.aspx");
                    break;
                case "Home":
                    Session["studentId"] = this.studentID.ToString();
                    Session["name"] = this.name;
                    Response.Redirect("StudentPage.aspx");
                    break;
                case "Lectures":
                    Session["studentId"] = this.studentID.ToString();
                    Session["name"] = this.name;
                    Session["sender"] = "studentPage";
                    Response.Redirect("LectureList.aspx");
                    break;
                default:
                    this.clearSessionAndCache();
                    Response.Redirect("LoginPage.aspx");
                    break;
            }
        }

        protected void clearSessionAndCache()
        {

            Session.Clear();
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            Response.AddHeader("Pragma", "no-cache");
            Response.AddHeader("Expires", "0");
            Response.Cache.SetValidUntilExpires(false);

        }

        protected void populateLecturesTable()
        {
            this.StudentName.Text = this.name;
            try
            {
                var studentLecturesQry = from studentLectures in this.databaseEntity.CurrentLectures.AsEnumerable()
                                         join students in this.databaseEntity.Students on
                                         this.studentID equals students.id_Student
                                         join lectures in this.databaseEntity.Lectures on
                                         studentLectures.id_lecture equals lectures.id_lecture
                                         join profesors in this.databaseEntity.Profesors on
                                         studentLectures.id_Profesor equals profesors.id_Profesor
                                         where this.studentID.Equals(studentLectures.id_student)
                                         select new { Lecture = lectures.name_lecture, Profesor = profesors.last_name };

                this.GridViewUserLectures.DataSource = studentLecturesQry.ToList();
                this.GridViewUserLectures.DataBind();
            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}