﻿insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(1, 'Programming 1','Computer Science');
insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(2, 'Databases 1', 'Computer Science');
insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(3, '.NET Technologies', 'Computer Science');
insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(4, 'Linear algebra', 'Mathematics');
insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(5, 'Discrete mathematics', 'Mathematics');
insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(6, 'Calculus', 'Mathematics');
insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(7, 'Business  1', 'Business and Management');
insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(8, 'Managment 1', 'Business and Management');
insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(9, 'Corporate integration', 'Business and Management');
insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(10, 'Design 1', 'Design');
insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(11, 'Architecture 1', 'Design');
insert into dbo.Lectures(id_lecture,name_lecture,faculty) values(12, 'Digital Design', 'Design');

insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(10, 'Hans','Maerta','hrt@test.com','Computer Science');
insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(11, 'Jane','Branson','jbrs@test.com','Computer Science');
insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(12, 'Peter','Parker','pprk@test.com','Computer Science');
insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(13, 'Mila', 'Reou', 'mre@test.com','Mathematics');
insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(14, 'Simon', 'Pe', 'spe@test.com','Mathematics');
insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(15, 'Soman', 'Repitov', 'srp@test.com','Mathematics');
insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(16, 'John', 'Mcarthy', 'jmr@test.com','Business and Management');
insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(17, 'Sara John', 'Maier', 'sjm@test.com','Business and Management');
insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(18, 'James', 'Johnson Jr', 'jjjr@test.com','Business and Management');
insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(19, 'Sara', 'Musterfraun', 'smf@test.com','Design');
insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(20, 'Jana', 'Schneider', 'jsch@test.com','Design');
insert into dbo.Profesors(id_Profesor,first_name,last_name,email,faculty) values(21, 'Ron', 'Trebix', 'rtr@test.com','Design');

insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(1,11);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(1,10);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(2,12);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(3,10);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(4,13);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(5,14);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(5,15);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(6,13);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(7,16);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(8,17);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(9,18);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(10,19);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(11,19);
insert into dbo.ProfesorAndLecture(id_lecture,id_Profesor) values(12,20);


