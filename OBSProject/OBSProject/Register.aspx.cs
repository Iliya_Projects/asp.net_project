﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OBSProject
{

    public partial class Register : System.Web.UI.Page
    {
        private struct StudentStrcut
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string major { get; set; }
            public string eMail { get; set; }
            public string password { get; set; }
            public int studentId { get; set; }
        };
        private StudentStrcut currentStudent;
        private int lastStudentIdInDatabase;
        OBS_DatabaseEntities databaseEntity;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //check if we already posted this
                if (Session["id"].Equals("posted"))
                {
                    //if yes than clear the cache
                    HttpResponse.RemoveOutputCacheItem("/Register.aspx");
                    Session["id"] = "clear";
                    Response.Redirect("Register.aspx");
                }
                else
                {
                    this.databaseEntity = new OBS_DatabaseEntities();
                    this.fillDropDownWihtMajors();
                    this.getLastStudentID();
                }
            }
            catch (Exception)
            {
                HttpResponse.RemoveOutputCacheItem("/Register.aspx");

                Response.Redirect("Register.aspx");
            }


        }
        protected void fillDropDownWihtMajors()
        {
            var getListWithMajors = (from list in this.databaseEntity.Lectures
                                     select list.faculty).Distinct();

            this.DropDownListMajors.DataSource = getListWithMajors.ToList();
            this.DropDownListMajors.DataBind();
        }
        protected void getLastStudentID()
        {

            var getLastStudentId = from studentIds in this.databaseEntity.Students
                                   select studentIds.id_Student;
            var listIds = getLastStudentId.ToList();

            if (listIds.Any())
            {
                this.lastStudentIdInDatabase = listIds.Last();

            }
            else
            {
                this.lastStudentIdInDatabase = 100;
            }


        }
        protected void Register_Event(object sender, EventArgs e)
        {
            //check if we already posted this
            if (!Session["id"].Equals("posted"))
            {       

                this.currentStudent = new StudentStrcut();
                this.getDataFromTextBoxes();
                this.createStudentID();
                this.saveToDataBase();
                Session["id"] = "posted";
            }
        }

        protected void getDataFromTextBoxes()
        {
            this.getNames();
            this.getMajor();
            this.getEmail();
            this.getPassword();
            this.cleanUpInputFromStudent();
        }

        protected void cleanUpInputFromStudent()
        {
            this.TextBoxFirstName.Text = "";
            this.TextBoxLastName.Text = "";
            this.DropDownListMajors.SelectedIndex = 0;
            this.TextBoxEmail.Text = "";

        }

        protected void saveToDataBase()
        {
            try
            {
                Student insertStudent = new Student();
                insertStudent.first_name = this.currentStudent.firstName;
                insertStudent.last_name = this.currentStudent.lastName;
                insertStudent.email = this.currentStudent.eMail;
                insertStudent.major = this.currentStudent.major;
                insertStudent.id_Student = this.currentStudent.studentId;
                insertStudent.pwd = this.currentStudent.password;
                this.databaseEntity.Students.Add(insertStudent);
                this.databaseEntity.SaveChanges();
                this.showStudentID();
            }
            catch (Exception exc)
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "error",
                    "<script language=javascript>alert('Unable to save student information. Please try again.');</script>");
                this.LabelStudentID.Text = " Unable to save student information.";
            }
        }

        protected void getNames()
        {
            this.currentStudent.firstName = this.TextBoxFirstName.Text;
            this.currentStudent.lastName = this.TextBoxLastName.Text;

        }

        protected void getMajor()
        {

            this.currentStudent.major = this.DropDownListMajors.SelectedItem.Text;
        }

        protected void getEmail()
        {
            this.currentStudent.eMail = this.TextBoxEmail.Text;
        }

        protected void getPassword()
        {
            //should I hash the value ???
            this.currentStudent.password = this.TextBoxPassword.Text;

        }

        protected void createStudentID()
        {
            this.lastStudentIdInDatabase++;
            this.currentStudent.studentId = this.lastStudentIdInDatabase;

        }

        protected void showStudentID()
        {
            this.LabelStudentID.Text = this.lastStudentIdInDatabase.ToString();

        }

        protected void Cancel_Event(object sender, EventArgs e)
        {
            HttpResponse.RemoveOutputCacheItem("/Register.aspx");
            this.cleanUpInputFromStudent();
            //Response.Redirect("LoginPage.aspx");
        }
    }
}