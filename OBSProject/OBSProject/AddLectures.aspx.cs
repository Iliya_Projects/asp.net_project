﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OBSProject
{
    public partial class MyLectures : System.Web.UI.Page
    {
        private struct lectureProfIds
        {
            public int profesorId { get; set; }
            public int lectureId { get; set; }
        };
        private int studentID;
        private OBS_DatabaseEntities databaseEntity;
        private string name;
        private List<lectureProfIds> listOfLectureAndProfsIds;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.databaseEntity = new OBS_DatabaseEntities();
                this.editMasterPageMenu();
                this.getStudentIDAndName();
                if (!IsPostBack)
                {
                    this.fillTableWithLectures();

                }

            }
            catch (Exception er)
            {

                Console.WriteLine(er.Message);
                ClientScript.RegisterClientScriptBlock(this.GetType(), "error",
                    "<script language=javascript>alert('Error on add lectures page.');</script>");
            }
        }

        protected void editMasterPageMenu()
        {
            MasterPage myMasterPage = (MasterPage)Page.Master;
            Menu masterMenu = (Menu)myMasterPage.FindControl("Menu1");
            MenuItem myItem = masterMenu.FindItem("HomeItem");
            MenuItem lecturesMenuItem = masterMenu.FindItem("LecturesItem");
            myItem.NavigateUrl = "";
            lecturesMenuItem.NavigateUrl = "";
            masterMenu.MenuItemClick += MasterMenu_MenuItemClick;

        }

        private void MasterMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            string command = e.Item.Text;
            switch (command)
            {
                case "Home":
                    this.clearSessionAndCache();
                    Session["studentId"] = this.studentID.ToString();
                    Session["name"] = this.name;
                    Response.Redirect("StudentPage.aspx");
                    break;
                /*addlecture is as well behind the logined */
                case "Lectures":
                    Session["studentId"] = this.studentID.ToString();
                    Session["name"] = this.name;
                    Session["sender"] = "studentPage";
                    Response.Redirect("LectureList.aspx");
                    break;
                default:
                    this.clearSessionAndCache();
                    Response.Redirect("LoginPage.aspx");
                    break;
            }
        }

        protected void getStudentIDAndName()
        {
            try
            {
                this.studentID = int.Parse(Session["studentId"].ToString());
                this.name = Session["name"].ToString();
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void fillTableWithLectures()
        {
            var getLecturesForMajor = (from lecturesForMajor in this.databaseEntity.Lectures
                                       join student in this.databaseEntity.Students on this.studentID equals
                                       student.id_Student
                                       join lectures in this.databaseEntity.Lectures on student.major equals
                                       lectures.faculty
                                       join lecturesAndProfs in this.databaseEntity.ProfesorAndLectures on
                                       lectures.id_lecture equals lecturesAndProfs.id_lecture
                                       select new
                                       {
                                           LectureList = lectures.name_lecture,
                                           ProfessorList = lecturesAndProfs.Profesor.last_name,
                                           ProfessorsId = lecturesAndProfs.id_Profesor,
                                           LectureId = lecturesAndProfs.id_lecture
                                       }).Distinct();

            this.GridView1.DataSource = getLecturesForMajor.ToList();
            this.GridView1.DataBind();
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            this.listOfLectureAndProfsIds = new List<lectureProfIds>();
            foreach (GridViewRow myRow in this.GridView1.Rows)
            {
                CheckBox t = (CheckBox)myRow.FindControl("CheckBox1");

                if (t != null && t.Checked)
                {
                    lectureProfIds newList = new lectureProfIds();
                    newList.profesorId = int.Parse(this.GridView1.DataKeys[myRow.RowIndex].Value.ToString());
                    newList.lectureId = int.Parse(this.GridView1.DataKeys[myRow.RowIndex].Values[1].ToString());
                    bool duplicate = false;
                    this.checkIfAlreadySignedForLecture(ref duplicate, newList.lectureId);

                    if (duplicate == false)
                    {
                        this.listOfLectureAndProfsIds.Add(newList);
                    }
                    else
                    {
                        this.clearSessionAndCache();
                        this.errorMessageJS("You are already signed for this lecture");
                        return;
                    }

                }
            }
            this.saveToDataBase();
        }

        protected void errorMessageJS(string message)
        {
            string jsPart = "<script language=javascript>alert('" + message + "');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "error",
                    jsPart);
        }
        protected void saveToDataBase()
        {
            if (this.listOfLectureAndProfsIds != null && this.listOfLectureAndProfsIds.Count > 0)
            {
                try
                {
                    CurrentLecture myLecture = new CurrentLecture();
                    foreach (var item in this.listOfLectureAndProfsIds)
                    {

                        myLecture.id_lecture = item.lectureId;
                        myLecture.id_Profesor = item.profesorId;
                        myLecture.id_student = this.studentID;
                        this.databaseEntity.CurrentLectures.Add(myLecture);
                        this.databaseEntity.SaveChanges();
                    }

                    //redirect
                    this.clearSessionAndCache();
                    Session["studentId"] = this.studentID.ToString();
                    Session["name"] = this.name;
                    Response.Redirect("StudentPage.aspx");
                }
                catch (Exception exc)
                {

                    Console.WriteLine(exc.Message);
                }
            }
        }
        protected void clearSessionAndCache()
        {

            Session.Clear();
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            Response.AddHeader("Pragma", "no-cache");
            Response.AddHeader("Expires", "0");
            Response.Cache.SetValidUntilExpires(false);

        }


        protected void checkIfAlreadySignedForLecture(ref bool duplicated, int id_lecture)
        {
            var getLecturesForStudent = from studentLecture in this.databaseEntity.CurrentLectures.AsEnumerable()
                                        where this.studentID.Equals(studentLecture.id_student) &&
                                        id_lecture.Equals(studentLecture.id_lecture)
                                        select studentLecture.id_lecture;

            try
            {
                var list = getLecturesForStudent.ToList();
                if (list.Count <= 0)
                {
                    duplicated = false;
                }
                else
                {
                    duplicated = true;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }
    }

}