﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="LectureList.aspx.cs" Inherits="OBSProject.LectureList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid pageContainterMargin">
        <asp:GridView ID="GridLecturesAndProfs" runat="server" AllowPaging="True" OnPageIndexChanging="GridLecturesAndProfs_PageIndexChanged" CssClass="table table-responsive">
        </asp:GridView>
    </div>
</asp:Content>
