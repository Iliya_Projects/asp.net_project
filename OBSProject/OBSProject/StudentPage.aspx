﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="StudentPage.aspx.cs" Inherits="OBSProject.StudentPage" %>
<%@ Reference VirtualPath="/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid pageContainterMargin">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Welcome: "></asp:Label>
            <asp:Label ID="StudentName" runat="server"></asp:Label>
        </div>
        <div class="container-fluid">
            <asp:GridView ID="GridViewUserLectures" CssClass="table" runat="server">
            </asp:GridView>
        </div>
    </div>

</asp:Content>
