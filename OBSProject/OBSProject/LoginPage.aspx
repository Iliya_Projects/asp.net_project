﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="OBSProject.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid pageContainterMargin">
        <div class=" container-fluid row">
            <asp:Label ID="LabelSutdentId" runat="server" EnableTheming="True" Text="Student_ID:"></asp:Label>
            <asp:TextBox ID="TextBoxStudentID" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxStudentID" Display="Dynamic" ErrorMessage="Field cannot be empty"></asp:RequiredFieldValidator>
            &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidatorID" runat="server" ControlToValidate="TextBoxStudentID" ErrorMessage="Wrong ID" ValidationExpression="^\d{3}"></asp:RegularExpressionValidator>
            <br />
        </div>
        <div class="container-fluid row">
            <asp:Label ID="LabelPassword" runat="server" Text="Password: "></asp:Label>
            &nbsp;<asp:TextBox ID="TextBoxPassword" runat="server" CssClass="TextBox" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPwd" runat="server" ControlToValidate="TextBoxPassword" ErrorMessage="Field cannot be empty"></asp:RequiredFieldValidator>
            <br />
            <style>
                .TextBox {
                    margin: 11px;
                }

                .ASPButton {
                    margin: 5px;
                }

            </style>
        </div>
        <div class="container-fluid row">
            <div>
                <asp:Button ID="ButtonLogin" runat="server" CssClass="ASPButton" Text="Login" OnClick="Login_Event" />
                <asp:Button ID="ButtonRegister" runat="server" Text="Register" CausesValidation="False" OnClick="Register_Event" />
            </div>
            
        </div>
    </div>
</asp:Content>
