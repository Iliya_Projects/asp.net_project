﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="OBSProject.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container container-fluid">
        <div class="container-fluid">
            <asp:Label ID="Label1" runat="server" CssClass="col-md-2" Text="First name: "></asp:Label>
            <asp:TextBox ID="TextBoxFirstName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxFirstName" ErrorMessage="Field cannot be empty"></asp:RequiredFieldValidator>
        </div>
        <br />
        <div class="container-fluid">
            <asp:Label ID="Label2" runat="server" CssClass="col-md-2" Text="Last name: "></asp:Label>
            <asp:TextBox ID="TextBoxLastName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxLastName" ErrorMessage="Field cannot be empty"></asp:RequiredFieldValidator>
        </div>
        <br />
        <div class="container-fluid">
            <asp:Label ID="Label3" runat="server"  CssClass="col-md-2" Text="Select your major : "></asp:Label>
            <asp:DropDownList ID="DropDownListMajors" AppendDataBoundItems="true" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListMajors" ErrorMessage="You must select your major"></asp:RequiredFieldValidator>
        </div>
        <br />
        <div class="container-fluid">
            <asp:Label ID="Label7" runat="server"  CssClass="col-md-2" Text="E-Mail:"></asp:Label>
            <asp:TextBox ID="TextBoxEmail" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Field cannto be empty" ControlToValidate="TextBoxEmail"></asp:RequiredFieldValidator>
            &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="TextBoxEmail" ErrorMessage="The input musst be an E-Mail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </div>
        <br />
        <div class="container-fluid">
            <asp:Label ID="Label4" runat="server"  CssClass="col-md-2" Text="Password: "></asp:Label>
            <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxPassword" ErrorMessage="Field cannot be empty"></asp:RequiredFieldValidator>
        </div>
        <br />
        <div class="container-fluid">
            <asp:Label ID="Label5" runat="server" CssClass="col-md-2" Text="Re enter Password: "></asp:Label>
            <asp:TextBox ID="TextBoxPassword2" runat="server" Height="25px" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxPassword2" ErrorMessage="Field cannot be empty"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBoxPassword" ErrorMessage="Passwords do not match" ControlToValidate="TextBoxPassword2"></asp:CompareValidator>
        </div>
        <br />
        <div class="container-fluid">
            <asp:Label ID="Label6" runat="server"  CssClass="col-md-2" Text="Your Student ID is:"></asp:Label>
            &nbsp;
            <asp:Label ID="LabelStudentID" runat="server"  CssClass="col-md-2" ></asp:Label>
        </div>
        <br />
        <div class="container-fluid">
            <p class="text-warning">Please save your Student ID, you will need it to log in later.</p>
        </div>

        <asp:Button ID="ButtonRegister" runat="server" Text="Register" OnClick="Register_Event" />
        <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CausesValidation="False" OnClick="Cancel_Event" />
        <br />
        <br />
    </div>
</asp:Content>
