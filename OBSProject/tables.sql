﻿create table Students(
	id_Student INT,
	first_name varchar(256),
	last_name varchar(256),
	email varchar(256),
	major varchar(256),
	primary key(id_Student)
)

create table Profesors(
	id_Profesor int,
	first_name varchar(256),
	last_name varchar(256),
	email varchar(256),
	faculty varchar(256),
	primary key(id_Profesor)
)

create table Lectures(
	id_lecture int,
	name_lecture varchar(256),
	faculty varchar(256),
	primary key(id_lecture)
)

create table ProfesorAndLecture(
	id int not null identity,
	id_lecture int,
	id_Profesor int,
	primary key(id),
	foreign key(id_lecture) references Lectures,
	foreign key(id_Profesor) references Profesors
)

create table CurrentLectures(
	id int not null identity,
	id_student int,
	id_lecture int,
	id_Profesor int,
	primary key(id),
	foreign key(id_student) references Students,
	foreign key(id_lecture) references Lectures,
	foreign key(id_Profesor) references Profesors
)